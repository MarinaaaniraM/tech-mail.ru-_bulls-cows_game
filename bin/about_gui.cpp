#include "about_gui.h"
#include "ui_about_gui.h"

About_GUI::About_GUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About_GUI)
{
    ui->setupUi(this);
    this->setFixedSize(400, 250);
    ui->gridLayout->setMargin(0);
}

About_GUI::~About_GUI()
{
    delete ui;
}
