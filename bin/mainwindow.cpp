#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedWidth(500);
    this->setMinimumHeight(500);

    // ------------------------------------------------------------------------
    settings = Settings::getObject(this);
    player = Player::getObject(this);
    analisis = new Algorithm(this);
    topPlayersWindow = new TopPlayers_GUI();
    playerWindow = new Player_GUI();
    settingsWindow = new Settings_GUI();
    aboutWindow = new About_GUI();
    message = new QMessageBox(this);
    playerWindow->setPlayerObject(player);
    settingsWindow->setSettingsObject(settings);
    timer = new QTimer(this);
    time = new QTime(0, 0);


    ui->gridLayout->setMargin(0);
    ui->gridLayout_2->setMargin(0);


    // ------------------------------------------------------------------------
    player->setCurrentPlayer(settings->getCurrentPlayer());

    // ------------------------------------------------------------------------
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimer()));
    connect(settingsWindow, SIGNAL(settingsAreChanged()), this, SLOT(startNewGame()));
    connect(playerWindow, SIGNAL(playerChanged()), this, SLOT(startNewGame()));

    // ------------------------------------------------------------------------
    startNewGame();
}

MainWindow::~MainWindow()
{
    delete aboutWindow;
    delete topPlayersWindow;
    delete playerWindow;
    delete settingsWindow;
    delete ui;
}


void MainWindow::updateTimer()
{
    *time = time->addSecs(1);

    ui->timeLabel->setText(time->toString(" HH:mm:ss"));
}


// ============================================================================
// Интерфейс.

// ----------------------------------------------------------------------------
void MainWindow::on_actionSettings_triggered()
{

    settingsWindow->show();
}


// ----------------------------------------------------------------------------
void MainWindow::on_actionPlayer_triggered()
{
    playerWindow->show();
}


// ----------------------------------------------------------------------------
void MainWindow::on_actionNew_triggered()
{
    int choice = message->question(this,
                              "Question",
                                  "Do you want to start a new game?",
                              QMessageBox::Yes, QMessageBox::No);

    if (choice == QMessageBox::Yes)
        startNewGame();
}


// ----------------------------------------------------------------------------
void MainWindow::on_actionTop_triggered()
{
    topPlayersWindow->myShow();
}



// ----------------------------------------------------------------------------
void MainWindow::on_actionAbout_triggered()
{
    aboutWindow->show();
}


// ----------------------------------------------------------------------------
void MainWindow::on_enterButton_clicked()
{
    if (ui->variantLine->text().length() < ui->variantLine->maxLength())
    {
        QMessageBox::warning(this, "Warning", "Wrong number");
    }
    else
    {
        processingResult();
    }
}


// ----------------------------------------------------------------------------
void MainWindow::on_logOutButton_clicked()
{
    int choice = QMessageBox::question(this,
                              "Question", "Do you want to start a new game?",
                              QMessageBox::Yes, QMessageBox::No);
    if (choice == QMessageBox::Yes)
    {
        player->setCurrentPlayer("");
        startNewGame();
    }
}


// ----------------------------------------------------------------------------
void MainWindow::on_actionExit_triggered()
{
    if (!ui->movesHistoryArea->toPlainText().isEmpty())
    {
        int choice = QMessageBox::question(this,
                                  "Question", "Do you really want exit?",
                                  QMessageBox::Yes, QMessageBox::No);
        if (choice == QMessageBox::Yes)
            this->close();
    }
    else
        this->close();
}


// ============================================================================
// Сама обработка.
void MainWindow::startNewGame()
{
    laps = 0;
    installConfiguration();
    ui->playerLabel->setText(player->getCurrentPlayer());
    ui->movesHistoryArea->clear();
    ui->bullsHistoryArea->clear();
    ui->cowsHistoryArea->clear();
    ui->variantLine->clear();
    time->setHMS(0, 0, 0);
    timer->start(1000);
    analisis->makeRandomNumber();
}


// ----------------------------------------------------------------------------
void MainWindow::installConfiguration()
{
    ui->BullsWidget->setFixedWidth(54);
    ui->CowsWidget->setFixedWidth(54);

    settings->getSettings();
    if (settings->getIsFiveNumbersMode())
    {
        ui->variantLine->setMaxLength(5);
        ui->variantLine->setFixedWidth(195);
        ui->movesHistoryArea->setFixedWidth(195);
    }
    else
    {
        ui->variantLine->setMaxLength(4);
        ui->variantLine->setFixedWidth(155);
        ui->movesHistoryArea->setFixedWidth(155);
    }

    ui->timeLabel->setHidden(!settings->getIsTimeEnabledMode());
}


// ----------------------------------------------------------------------------
void MainWindow::processingResult()
{
    ++ laps;
    bool isWin = analisis->findBullsAndCows(ui->variantLine->text().toUInt());
    ui->movesHistoryArea->append(ui->variantLine->text());
    ui->bullsHistoryArea->append(QString::number(analisis->getBulls()));
    ui->cowsHistoryArea->append(QString::number(analisis->getCows()));

    if (isWin)
    {
        timer->stop();
        player->addPlayerResult(laps, time->second());
        QMessageBox::information(this, "Congratulations", "YOU WIN! :)");
    }
    ui->variantLine->clear();
}



// ============================================================================
void MainWindow::closeEvent(QCloseEvent*)
{
    settings->setCurrentPlayer(player->getCurrentPlayer());
    settings->setSettings();
    topPlayersWindow->close();
    playerWindow->close();
    settingsWindow->close();
}















