#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <QObject>
#include <QDebug>
#include <QTime>
#include <QFile>
#include <QDomDocument>

#define CONFIG_FILE "config.xml"

class Algorithm : public QObject
{
    Q_OBJECT
public:
    explicit Algorithm(QObject *parent = 0);
    ~Algorithm();

    bool findBullsAndCows(quint32 number);

    int getBulls() const
    {
        return bullsAndCows[0];
    }
    int getCows() const
    {
        return bullsAndCows[1];
    }
    
    void makeRandomNumber();

private:
    void gettingsettings();

    int* bullsAndCows;
    int* magicNumbers;
    int quantityNumbersMode;
    
};

#endif // ALGORITHM_H
