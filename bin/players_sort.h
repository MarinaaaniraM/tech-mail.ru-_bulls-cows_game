#ifndef PLAYERS_SORT_H
#define PLAYERS_SORT_H

#include <QVector>
#include <QString>
#include <QDebug>

#include <player.h>
#include <dom_iterator.h>

#define TOP_PLAYERS 10

class Players_Sort
{
public:
    Players_Sort();

    QVector <QVector<QString> > topBy(int indexSort);
    enum
    {
        LAPS = 1,
        TIME
    };
private:
    void merge (QVector <QVector<QString> >& v, int first, int last, int indexSort);
    void mergeTop (QVector <QVector<QString> >& v1,
                   QVector <QVector<QString> >& v2,
                   int indexSort);
    void mergeSort (QVector <QVector<QString> >& v, int first, int last, int indexSort);


};

#endif // PLAYERS_SORT_H
