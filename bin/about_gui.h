#ifndef ABOUT_GUI_H
#define ABOUT_GUI_H

#include <QWidget>

namespace Ui {
class About_GUI;
}

class About_GUI : public QWidget
{
    Q_OBJECT
    
public:
    explicit About_GUI(QWidget *parent = 0);
    ~About_GUI();
    
private:
    Ui::About_GUI *ui;
};

#endif // ABOUT_GUI_H
