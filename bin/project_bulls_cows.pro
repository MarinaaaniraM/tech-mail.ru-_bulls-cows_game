#-------------------------------------------------
#
# Project created by QtCreator 2014-03-17T13:30:12
#
#-------------------------------------------------

QT       += core gui \
            xml

TARGET = project_bulls_cows
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    algorithm.cpp \
    player.cpp \
    player_gui.cpp \
    settings.cpp \
    settings_gui.cpp \
    iterator.cpp \
    dom_iterator.cpp \
    players_sort.cpp \
    topplayers_gui.cpp \
    about_gui.cpp

HEADERS  += mainwindow.h \
    algorithm.h \
    player.h \
    player_gui.h \
    settings.h \
    settings_gui.h \
    iterator.h \
    dom_iterator.h \
    players_sort.h \
    topplayers_gui.h \
    about_gui.h

FORMS    += \
    mainwindow.ui \
    player_gui.ui \
    settings_gui.ui \
    topplayers_gui.ui \
    about_gui.ui

RESOURCES += \
    ../resources/resources.qrc
