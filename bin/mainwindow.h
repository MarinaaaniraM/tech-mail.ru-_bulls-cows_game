#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include <QTime>
#include <QDebug>

#include <settings.h>
#include <player.h>
#include <settings_gui.h>
#include <player_gui.h>
#include <algorithm.h>
#include <players_sort.h>
#include <topplayers_gui.h>
#include <about_gui.h>

#define CONFIG_FILE "config.xml"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionSettings_triggered();
    void on_enterButton_clicked();
    void on_actionNew_triggered();
    void on_actionPlayer_triggered();
    void on_actionTop_triggered();
    void on_logOutButton_clicked();
    void on_actionExit_triggered();

    void startNewGame();
    void updateTimer();

    void on_actionAbout_triggered();

private:

    void closeEvent(QCloseEvent*);
    void installConfiguration();
    void processingResult();

    Ui::MainWindow *ui;
    Player* player;
    Settings* settings;
    Algorithm* analisis;
    Player_GUI* playerWindow;
    Settings_GUI* settingsWindow;
    TopPlayers_GUI* topPlayersWindow;
    QMessageBox* message;
    QTimer* timer;
    QTime* time;
    About_GUI* aboutWindow;
    int laps;
};

#endif // MAINWINDOW_H
