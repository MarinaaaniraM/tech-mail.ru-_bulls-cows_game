#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QString>
#include <QDomDocument>
#include <QFile>
#include <QTextStream>
#include <QVector>
#include <QDebug>

#define PLAYERS_FILE "players.xml"

class Player : public QObject
{
    Q_OBJECT
public:
    static Player* getObject(QObject *parent = 0)
    {
        static Player* singleton = NULL;
        if (singleton == NULL)
        {
            singleton = new Player(parent);
        }
        return singleton;
    }
    void setCurrentPlayer(QString name)
    {
        currentPlayer = name;
    }
    QString getCurrentPlayer() const
    {
        return currentPlayer;
    }
    QDomDocument getPlayersDom() const;
    bool addNewPlayer(QString name);
    void addPlayerResult(quint32 laps, quint32 sec);
    bool noPlayers();    

private:
    explicit Player(QObject *parent = 0);
    QString currentPlayer;
};

#endif // PLAYER_H
