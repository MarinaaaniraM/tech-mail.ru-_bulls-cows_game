#include "settings_gui.h"
#include "ui_settings_gui.h"

Settings_GUI::Settings_GUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings_GUI)
{
    ui->setupUi(this);
    ui->gridLayout->setMargin(0);
    this->setFixedSize(300, 200);
    pal = new QPalette();
    pal->setColor(QPalette::ButtonText, Qt::white);
    ui->fiveModeRadio->setPalette(*pal);

}

Settings_GUI::~Settings_GUI()
{
    delete ui;
}


void Settings_GUI::setSettingsObject(Settings *set)
{
    settings = set;

    ui->fiveModeRadio->setChecked(settings->getIsFiveNumbersMode());
    ui->fourModeRadio->setChecked(!settings->getIsFiveNumbersMode());
    ui->timeEnabledCheck->setChecked(settings->getIsTimeEnabledMode());
}

void Settings_GUI::on_settingsOkButton_clicked()
{
    int choice = QMessageBox::question(this,
                              "Question", "Do you want to start a new game"
                              " with this settings?",
                              QMessageBox::Yes, QMessageBox::No);
    if (choice == QMessageBox::Yes)
    {
        settings->setIsFiveNumbersMode(ui->fiveModeRadio->isChecked());
        settings->setIsTimeEnabledMode(ui->timeEnabledCheck->isChecked());
        settings->setSettings();

        emit settingsAreChanged();
        this->close();
    }
}
