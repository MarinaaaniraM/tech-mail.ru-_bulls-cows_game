#ifndef ITERATOR_H
#define ITERATOR_H

template <class Item>
class Iterator
{
public:
    virtual void first() = 0;
    virtual void next() = 0;
    virtual bool isDone() const = 0;
    virtual Item current() const = 0;
protected:
    Iterator();
};

#endif // ITERATOR_H
