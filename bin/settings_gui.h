#ifndef SETTINGS_GUI_H
#define SETTINGS_GUI_H

#include <QWidget>
#include <QMessageBox>
#include <QPalette>

#include <settings.h>

namespace Ui {
class Settings_GUI;
}

class Settings_GUI : public QWidget
{
    Q_OBJECT
    
public:
    explicit Settings_GUI(QWidget *parent = 0);
    ~Settings_GUI();
    void setSettingsObject(Settings *set);

private slots:
    void on_settingsOkButton_clicked();

signals:
    void settingsAreChanged();

private:
    Ui::Settings_GUI *ui;

    Settings* settings;
    QPalette* pal;
};

#endif // SETTINGS_GUI_H
