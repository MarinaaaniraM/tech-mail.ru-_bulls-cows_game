#include "players_sort.h"

Players_Sort::Players_Sort()
{

}


// ----------------------------------------------------------------------------
void Players_Sort::merge(QVector <QVector<QString> >& v, int first, int last, int indexSort)
{
    int posLeft = first;
    int middle = (first + last) / 2;
    int posRight = middle + 1;
    QVector <QVector<QString> > tempArray;

    while (posLeft <= middle && posRight <= last)
    {
        if (v[posLeft][indexSort].toUInt() <= v[posRight][indexSort].toUInt())
        {
            tempArray.push_back(v[posLeft++]);
        }
        else if (v[posLeft][indexSort].toUInt() > v[posRight][indexSort].toUInt())
        {
            tempArray.push_back(v[posRight++]);
        }
    }
    while (posRight <= last)
    {
        tempArray.push_back(v[posRight++]);
    }
    while (posLeft <= middle)
    {
        tempArray.push_back(v[posLeft++]);
    }

    int i = 0;
    for (int j = first; j <= last; ++j)
        v[j] = tempArray[i++];
}


// ----------------------------------------------------------------------------
void Players_Sort::
mergeSort(QVector <QVector<QString> > &v, int first, int last, int indexSort)
{
    if (first < last)
    {
        mergeSort(v, first, (first + last) / 2, indexSort);
        mergeSort(v, (first + last) / 2 + 1, last, indexSort);
        merge(v, first, last, indexSort);
    }
}


// ----------------------------------------------------------------------------
void Players_Sort::
mergeTop(QVector <QVector<QString> > &v1,
         QVector <QVector<QString> > &v2, int indexSort)
{
    int posLeft = 0;
    int posRight = 0;
    QVector <QVector<QString> > tempArray;

    while (posLeft <= v1.size() - 1 && posRight <= v2.size() - 1)
    {
        if (v1[posLeft][indexSort].toUInt() <= v2[posRight][indexSort].toUInt())
        {
            tempArray.push_back(v1[posLeft++]);
        }
        else if (v1[posLeft][indexSort].toUInt() > v2[posRight][indexSort].toUInt())
        {
            tempArray.push_back(v2[posRight++]);
        }
    }
    while (posRight <= v2.size() - 1)
    {
        tempArray.push_back(v2[posRight++]);
    }
    while (posLeft <= v1.size() - 1)
    {
        tempArray.push_back(v1[posLeft++]);
    }

    for (int j = 0; j <= v1.size() - 1; ++j)
        v1[j] = tempArray[j];
}


// ----------------------------------------------------------------------------
QVector <QVector<QString> > Players_Sort::topBy(int indexSort)
{
    Player* player;
    player = Player::getObject();
    QVector <QVector<QString> > topBy;
    if (player->getPlayersDom().isNull())
    {
        return topBy;
    }
    Dom_Iterator <QVector<QString> > i (player->getPlayersDom());


    i.first();
    for (int k = 0; k < TOP_PLAYERS; ++k)
    {
        if (i.isDone())
            break;
        if (i.current()[indexSort] != "0")
            topBy.append(i.current());
        i.next();
    }
    mergeSort(topBy, 0, topBy.size() - 1, indexSort);

    while (!i.isDone())
    {
        QVector <QVector<QString> > temp;
        for (int k = 0; k < TOP_PLAYERS; ++k)
        {
            if (i.isDone())
                break;
            if (i.current()[indexSort] != " ")
                temp.append(i.current());
            i.next();
        }
        mergeSort(temp, 0, temp.size() - 1, indexSort);
        mergeTop(topBy, temp, indexSort);
    }
    return topBy;
}










