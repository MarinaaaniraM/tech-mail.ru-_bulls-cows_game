#include "algorithm.h"

Algorithm::Algorithm(QObject *parent) :
    QObject(parent)
{
    // ------------------------------------------------------------------------
    bullsAndCows = new int [2];
    magicNumbers = new int [quantityNumbersMode];


    // ------------------------------------------------------------------------
    QFile fileConfig(CONFIG_FILE);
    if (fileConfig.exists())
    {
        gettingsettings();
    }
    else
    {
        quantityNumbersMode = 4;
    }
}


// ----------------------------------------------------------------------------
Algorithm::~Algorithm()
{
    delete[] bullsAndCows;
    delete[] magicNumbers;
}


// ----------------------------------------------------------------------------
void Algorithm::makeRandomNumber()
{
    gettingsettings();
    for (int i = 0; i < quantityNumbersMode; ++i)
    {
        qsrand(QTime::currentTime().msec());
        magicNumbers[i] = qrand()%10;
        if (i == 0 && magicNumbers[i] == 0)
        {
            --i;
        }
        for (int k = 0; k < i; ++k)
        {
            if (magicNumbers[i] == magicNumbers[i - (k + 1)])
            {
                --i;
                break;
            }
        }
    }
    qDebug() << magicNumbers[0]
             << magicNumbers[1] << magicNumbers[2]
             << magicNumbers[3] << magicNumbers[4];
}


// ----------------------------------------------------------------------------
bool Algorithm::findBullsAndCows(quint32 number)
{
    bullsAndCows[0] = 0;
    bullsAndCows[1] = 0;
    int partOfNumber[quantityNumbersMode];
    for (int i = 0; i < quantityNumbersMode; ++i)
    {
        partOfNumber[i] = number%10;
        number = number/10;
        for (int k = 0; k < quantityNumbersMode; ++k)
        {
            if (partOfNumber[i] == magicNumbers[k])
            {
                if (i == ((quantityNumbersMode - 1) - k))
                {
                    bullsAndCows[0] ++;
                }
                else
                {
                    bullsAndCows[1] ++;
                }
            }
        }
    }
    return (bullsAndCows[0] == quantityNumbersMode) ? true : false;
}


// ----------------------------------------------------------------------------
void Algorithm::gettingsettings()
{
    QFile fileConfig(CONFIG_FILE);
    fileConfig.open(QIODevice::ReadOnly);
    QDomDocument domConfig;
    domConfig.setContent(&fileConfig);
    QDomElement currentConfig = domConfig.documentElement();
    QDomNode node = currentConfig.firstChild();

    quantityNumbersMode =
            node.toElement().attribute("isFiveNumbersMode").toUInt() ? 5 : 4;

    fileConfig.close();

    delete[] magicNumbers;
    magicNumbers = new int[quantityNumbersMode];
}













