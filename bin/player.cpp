#include "player.h"

Player::Player(QObject *parent) :
    QObject(parent)
{
    currentPlayer = " ";
}

// ----------------------------------------------------------------------------
bool Player::noPlayers()
{
    QFile file(PLAYERS_FILE);
    return !file.exists() ? true : false;
}


// ----------------------------------------------------------------------------
bool Player::addNewPlayer(QString name)
{
    QFile file(PLAYERS_FILE);
    QDomDocument domPlayers;
    QDomElement header;

    if (!file.exists())
    {
        file.open(QIODevice::WriteOnly);
        header = domPlayers.createElement("header");
        domPlayers.appendChild(header);
    }
    else
    {
        file.open(QIODevice::ReadWrite);
        domPlayers.setContent(&file);
        header = domPlayers.documentElement();
    }

    if (!domPlayers.elementsByTagName("player_" + name).item(0).toElement().tagName().isEmpty())
    {
        file.close();
        return false;
    }

    else
    {
        QDomElement player = domPlayers.createElement("player_" + name);
        player.setAttribute("name", name);
        player.setAttribute("laps", 0);
        player.setAttribute("time", 0);
        header.appendChild(player);

        file.seek(0);
        QTextStream(&file) << domPlayers.toString();

        currentPlayer = name;
        file.close();
        return true;
    }

}


// ----------------------------------------------------------------------------
void Player::addPlayerResult(quint32 laps, quint32 sec)
{
    QFile file(PLAYERS_FILE);
    if (file.exists())
    {
        file.open(QIODevice::ReadWrite);
        QDomDocument domPlayers;
        domPlayers.setContent(&file);
        QDomElement header = domPlayers.documentElement();

        QDomElement player = domPlayers.elementsByTagName("player_" + currentPlayer).item(0).toElement();

        if (player.attribute("time").toUInt() > sec ||
            player.attribute("time").toUInt() == 0)
                player.setAttribute("time", sec);

        if (player.attribute("laps").toUInt() > laps ||
            player.attribute("laps").toUInt() == 0)
                player.setAttribute("laps", laps);
        header.appendChild(player);

        file.seek(0);
        QTextStream(&file) << domPlayers.toString();

        file.close();
    }
}


// ----------------------------------------------------------------------------
QDomDocument Player::getPlayersDom() const
{
    QFile file(PLAYERS_FILE);
    if (file.exists())
    {
        file.open(QIODevice::ReadOnly);
        QDomDocument domPlayers;
        domPlayers.setContent(&file);

        file.close();
        return domPlayers;
    }
    QDomDocument empty;
    return empty;
}
















