#ifndef DOM_ITERATOR_H
#define DOM_ITERATOR_H

#include <iterator>
#include <QVector>
#include <QString>
#include <QDomDocument>

template <class Item>
class Dom_Iterator
{
public:
    Dom_Iterator(QDomDocument d) : dom(d)
    {
        element = dom.documentElement();
    }
    void first()
    {
        currentNode = element.firstChild();
    }
    void next()
    {
        currentNode = currentNode.nextSibling();
    }
    Item current() const
    {
        Item collection;
        collection.append(currentNode.toElement().attribute("name"));
        collection.append(currentNode.toElement().attribute("laps"));
        collection.append(currentNode.toElement().attribute("time"));
        return collection;
    }
    bool isDone() const
    {
        return currentNode.toElement().attribute("name").trimmed().isEmpty();
    }
private:
    QDomDocument dom;
    QDomElement element;
    QDomNode currentNode;

};

#endif // DOM_ITERATOR_H
