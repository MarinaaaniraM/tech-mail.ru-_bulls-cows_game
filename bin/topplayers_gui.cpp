#include "topplayers_gui.h"
#include "ui_topplayers_gui.h"

TopPlayers_GUI::TopPlayers_GUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TopPlayers_GUI)
{
    ui->setupUi(this);
    ui->gridLayout->setMargin(0);
    this->setFixedSize(400, 450);
}

TopPlayers_GUI::~TopPlayers_GUI()
{
    delete ui;
}

void TopPlayers_GUI::myShow()
{
    ui->sortByLapsRadio->setChecked(true);
    this->setMinimumSize(300, 250);
    ui->nameEdit->setMinimumWidth(70);
    ui->lapsEdit->setMinimumWidth(80);
    ui->timeEdit->setMinimumWidth(80);
    ui->lapsEdit->setMaximumWidth(100);
    ui->timeEdit->setMaximumWidth(100);
    ui->warningLabel->setHidden(true);
    printTopPlayers(Players_Sort::LAPS);
    this->show();
}

void TopPlayers_GUI::on_sortByTimeRadio_clicked()
{
    printTopPlayers(Players_Sort::TIME);
}

void TopPlayers_GUI::on_sortByLapsRadio_clicked()
{
    printTopPlayers(Players_Sort::LAPS);
}


void TopPlayers_GUI::printTopPlayers(int sortBy)
{
    QVector <QVector<QString> > topPlayers;
    Players_Sort sort;
    topPlayers = sort.topBy(sortBy);
    if (topPlayers.isEmpty())
    {
        warningWindow();
    }
    else
    {
        ui->nameEdit->clear();
        ui->lapsEdit->clear();
        ui->timeEdit->clear();
        for (int i = 0; i < topPlayers.size(); ++i)
        {
            ui->nameEdit->append(topPlayers[i][0]);
            ui->lapsEdit->append(topPlayers[i][1]);
            QTime time = time.addSecs(topPlayers[i][2].toUInt());
            ui->timeEdit->append(time.toString("HH:mm:ss"));
        }
    }
}


void TopPlayers_GUI::warningWindow()
{
    ui->sortByLapsRadio->setHidden(true);
    ui->sortByTimeRadio->setHidden(true);
    ui->nameEdit->setHidden(true);
    ui->lapsEdit->setHidden(true);
    ui->timeEdit->setHidden(true);

    ui->warningLabel->setHidden(false);
}
























