#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QDomDocument>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QDebug>

#define CONFIG_FILE "config.xml"

class Settings : public QObject
{
    Q_OBJECT
public:
    static Settings* getObject(QObject *parent = 0)
    {
        static Settings* singleton = NULL;
        if (singleton == NULL)
        {
            singleton = new Settings(parent);
        }
        return singleton;
    }

    bool getIsFiveNumbersMode() const
    {
        return isFiveNumbersMode;
    }
    bool getIsTimeEnabledMode() const
    {
        return isTimeEnabledMode;
    }
    QString getCurrentPlayer() const
    {
        return currentPlayer;
    }
    void setIsFiveNumbersMode(bool b)
    {
        isFiveNumbersMode = b;
    }
    void setIsTimeEnabledMode(bool b)
    {
        isTimeEnabledMode = b;
    }
    void setCurrentPlayer(QString plr)
    {
        currentPlayer = plr;
    }

    void setSettings();
    void getSettings();

private:
    explicit Settings(QObject *parent = 0);
    bool isFiveNumbersMode;
    bool isTimeEnabledMode;
    QString currentPlayer;
};

#endif // SETTINGS_H
