#ifndef TOPPLAYERS_GUI_H
#define TOPPLAYERS_GUI_H

#include <QWidget>
#include <QTime>

#include <players_sort.h>

namespace Ui {
class TopPlayers_GUI;
}

class TopPlayers_GUI : public QWidget
{
    Q_OBJECT
    
public:
    explicit TopPlayers_GUI(QWidget *parent = 0);
    ~TopPlayers_GUI();
    void myShow();
    
private slots:
    void on_sortByTimeRadio_clicked();
    void on_sortByLapsRadio_clicked();

private:
    Ui::TopPlayers_GUI *ui;

    void printTopPlayers (int sortBy);
    void warningWindow();
};

#endif // TOPPLAYERS_GUI_H
