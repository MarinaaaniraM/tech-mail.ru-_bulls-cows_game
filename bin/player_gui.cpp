#include "player_gui.h"
#include "ui_player_gui.h"

Player_GUI::Player_GUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Player_GUI)
{
    ui->setupUi(this);
    ui->gridLayout->setMargin(0);
    this->setFixedSize(400, 150);
}

Player_GUI::~Player_GUI()
{
    delete ui;
}

void Player_GUI::setPlayerObject(Player *pl)
{
    player = pl;

    if (player->noPlayers())
        createPlayerWindow();
    else
        choosePlayerWindow();
}

void Player_GUI::on_creatNewPlayerButton_clicked()
{
    createPlayerWindow();
}

void Player_GUI::on_newPlayerOkButton_clicked()
{
    QString newName = ui->newPlayerLine->text();
    if (player->addNewPlayer(newName))
        choosePlayerWindow();
    else
    {
        QMessageBox::warning(this, "Warning", "This name already exist.");
        createPlayerWindow();
        ui->newPlayerLine->setText(newName);
    }
}

void Player_GUI::on_newPlayerCancelButton_clicked()
{
        choosePlayerWindow();
}


void Player_GUI::on_choosePlayerOkButton_clicked()
{
    int choice = QMessageBox::question(this,
                              "Question", "Do you want to start a new game"
                              " with new player?",
                              QMessageBox::Yes, QMessageBox::No);
    if (choice == QMessageBox::Yes)
    {
        player->setCurrentPlayer(ui->choosePlayerBox->currentText());

        emit playerChanged();
        this->close();
    }
}

void Player_GUI::createPlayerWindow()
{
    ui->newPlayerLabel->setHidden(false);
    ui->newPlayerLine->setHidden(false);
    ui->newPlayerLine->clear();
    ui->newPlayerOkButton->setHidden(false);
    ui->newPlayerCancelButton->setHidden(false);

    ui->choosePlayerLabel->setHidden(true);
    ui->choosePlayerBox->setHidden(true);
    ui->creatNewPlayerButton->setHidden(true);
    ui->choosePlayerCancelButton->setHidden(true);
    ui->choosePlayerOkButton->setHidden(true);
}


void Player_GUI::choosePlayerWindow()
{
    ui->choosePlayerLabel->setHidden(false);
    ui->choosePlayerBox->setHidden(false);
    ui->creatNewPlayerButton->setHidden(false);
    ui->choosePlayerOkButton->setHidden(false);
    ui->choosePlayerCancelButton->setHidden(false);

    ui->newPlayerLabel->setHidden(true);
    ui->newPlayerLine->setHidden(true);
    ui->newPlayerOkButton->setHidden(true);
    ui->newPlayerCancelButton->setHidden(true);

    Dom_Iterator <QVector<QString> > i (player->getPlayersDom());
    ui->choosePlayerBox->clear();
    for (i.first(); !i.isDone(); i.next())
    {
        // В будущем надо просто добавлять!!!!!!
        ui->choosePlayerBox->addItem(i.current()[0]);
    }
}























