#include "settings.h"

Settings::Settings(QObject *parent) :
    QObject(parent)
{
    QFile fileConfig(CONFIG_FILE);
    if (fileConfig.exists())
    {
        getSettings();
    }
    else
    {
        isFiveNumbersMode = false;
        isTimeEnabledMode = true;
        currentPlayer = "";
    }
}

void Settings::setSettings()
{
    QFile fileConfig (CONFIG_FILE);
    QDomDocument domConfig;
    QDomElement header;

    fileConfig.open(QIODevice::WriteOnly);
    header = domConfig.createElement("config");
    domConfig.appendChild(header);

    QDomElement currentConfig = domConfig.createElement("currentConfig");
    currentConfig.setAttribute(
                "isFiveNumbersMode", isFiveNumbersMode);
    currentConfig.setAttribute("isTimeEnable", isTimeEnabledMode);
    currentConfig.setAttribute("currentPlayer", currentPlayer);
    header.appendChild(currentConfig);

    fileConfig.seek(0);
    QTextStream(&fileConfig) << domConfig.toString();

    fileConfig.close();
}


void Settings::getSettings()
{
    QFile fileConfig(CONFIG_FILE);
    if (fileConfig.exists())
    {
        fileConfig.open(QIODevice::ReadOnly);
        QDomDocument domConfig;
        domConfig.setContent(&fileConfig);
        QDomElement currentConfig = domConfig.documentElement();
        QDomNode node = currentConfig.firstChild();

        isFiveNumbersMode = node.toElement().attribute("isFiveNumbersMode").toUInt();
        isTimeEnabledMode = node.toElement().attribute("isTimeEnable").toUInt();
        currentPlayer = node.toElement().attribute("currentPlayer");

        fileConfig.close();
    }
}
