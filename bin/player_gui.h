#ifndef PLAYER_GUI_H
#define PLAYER_GUI_H

#include <QWidget>
#include <QString>
#include <QMessageBox>

#include <player.h>
#include <dom_iterator.h>

namespace Ui {
class Player_GUI;
}

class Player_GUI : public QWidget
{
    Q_OBJECT
    
public:
    explicit Player_GUI(QWidget *parent = 0);
    ~Player_GUI();
    void setPlayerObject(Player *pl);
    
private slots:
    void on_creatNewPlayerButton_clicked();
    void on_newPlayerOkButton_clicked();
    void on_newPlayerCancelButton_clicked();
    void on_choosePlayerOkButton_clicked();

signals:
    void playerChanged();

private:
    Ui::Player_GUI *ui;

    Player* player;
    void createPlayerWindow();
    void choosePlayerWindow();

};

#endif // PLAYER_GUI_H
